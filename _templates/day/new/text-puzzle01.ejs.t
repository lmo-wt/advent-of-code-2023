---
to: src/<%= name %>/__test__/puzzle01.test.ts
---
import solve from '../puzzle01'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual('solution')
})
