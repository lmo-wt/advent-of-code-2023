---
to: src/<%= name %>/puzzle01.ts
---
import parse from './parse'

const solve = inputPath => {
  const parsed = parse(inputPath)
  return 'solution'
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
