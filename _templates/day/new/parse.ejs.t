---
to: src/<%= name %>/parse.ts
---
import { readlines } from '../utils'

const parse = (inputPath: string): any => {
  const parsed = readlines(inputPath).map((line: string) => {
    return line
  })

  return parsed
}

export default parse
