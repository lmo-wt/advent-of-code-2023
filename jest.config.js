/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  roots: ['src'],
  testEnvironment: 'node',
  setupFilesAfterEnv: ['jest-extended'],
  coverageReporters: ['clover', 'json', 'lcov', 'text', 'cobertura'],
  testTimeout: 5000,
  verbose: false,
  reporters: ['default', './dist/JestReporter.js'],
  coveragePathIgnorePatterns: [
    'node_modules',
    '<rootDir>/src/JestReporter.ts',
  ],
}
