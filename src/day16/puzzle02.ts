import { Point } from '../utils'
import parse from './parse'
import { Direction, getEnergy } from './puzzle01'

const solve = inputPath => {
  const grid = parse(inputPath)

  let maxEnergy = 0
  for (let x = 0; x <= grid.xmax; x++) {
    maxEnergy = Math.max(maxEnergy, getEnergy(grid, new Point(x, grid.ymax + 1), Direction.Down))
    maxEnergy = Math.max(maxEnergy, getEnergy(grid, new Point(x, -1), Direction.Up))
  }
  for (let y = 0; y <= grid.ymax; y++) {
    maxEnergy = Math.max(maxEnergy, getEnergy(grid, new Point(-1, y), Direction.Right))
    maxEnergy = Math.max(maxEnergy, getEnergy(grid, new Point(grid.xmax + 1, y), Direction.Left))
  }
  return maxEnergy
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
