import { Grid, makeGrid, readlines } from '../utils'

export enum Tile {
  Empty = '.',
  EmptyEnergized = '#',
  HorizontalSplitter = '-',
  VerticalSplitter = '|',
  UpMirror = '/',
  DownMirror = '\\',
}

const parse = (inputPath: string): Grid<Tile> => {
  const values = readlines(inputPath).map((line: string) => line.split('').map((char: string) => char as Tile))
  const grid = makeGrid<Tile>(values)

  return grid
}

export default parse
