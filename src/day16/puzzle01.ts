import { Grid, Point, Vector } from '../utils'
import parse, { Tile } from './parse'

export enum Direction {
  Up = '^',
  Down = 'v',
  Left = '<',
  Right = '>',
}

export const DirectionVector = {
  [Direction.Up]: new Vector(0, 1),
  [Direction.Down]: new Vector(0, -1),
  [Direction.Left]: new Vector(-1, 0),
  [Direction.Right]: new Vector(1, 0),
}

export const getEnergy = (grid: Grid<Tile>, startingPoint: Point, startingDirection: Direction): number => {
  const energized = new Set<string>()
  const path = new Set<string>()
  const progress = (from: Point, direction: Direction): void => {
    const hash = `${from.hash()}${direction}`
    if (path.has(hash)) {
      return
    }
    path.add(hash)
    const position = from.translate(DirectionVector[direction])
    if (position.x < 0 || position.y < 0 || position.x > grid.xmax || position.y > grid.ymax) {
      return
    }
    if (!energized.has(position.hash())) {
      energized.add(position.hash())
    }
    const tile = grid.get(position.x, position.y)
    if (tile === Tile.Empty || tile === Tile.EmptyEnergized) {
      progress(position, direction)
    } else if (tile === Tile.HorizontalSplitter) {
      if (direction === Direction.Up || direction === Direction.Down) {
        progress(position, Direction.Left)
        progress(position, Direction.Right)
      } else {
        progress(position, direction)  
      }
    } else if (tile === Tile.VerticalSplitter) {
      if (direction === Direction.Left || direction === Direction.Right) {
        progress(position, Direction.Up)
        progress(position, Direction.Down)
      } else {
        progress(position, direction)    
      }
    } else if (tile === Tile.UpMirror) {
      if (direction === Direction.Up) {
        progress(position, Direction.Right)
      } else if (direction === Direction.Down) {
        progress(position, Direction.Left)
      } else if (direction === Direction.Left) {
        progress(position, Direction.Down)
      } else if (direction === Direction.Right) {
        progress(position, Direction.Up)
      }
    } else if (tile === Tile.DownMirror) {
      if (direction === Direction.Up) {
        progress(position, Direction.Left)
      } else if (direction === Direction.Down) {
        progress(position, Direction.Right)
      } else if (direction === Direction.Left) {
        progress(position, Direction.Up)
      } else if (direction === Direction.Right) {
        progress(position, Direction.Down)
      }
    }
  }

  progress(startingPoint, startingDirection)
  return energized.size
}

const solve = inputPath => {
  const grid = parse(inputPath)
  
  const solution = getEnergy(grid, new Point(-1, grid.ymax), Direction.Right)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
