import { sum } from '../utils'
import parse from './parse'
import { extrapolate } from './puzzle01'

const solve = inputPath => {
  const sequences = parse(inputPath)
  const nexts = sequences.map(sequence => extrapolate(sequence, true))

  return sum(nexts)
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
