import { readlines } from '../utils'

const parse = (inputPath: string): number[][] => {
  return readlines(inputPath).map((line: string) => line.split(' ').map((number: string) => parseInt(number)))
}

export default parse
