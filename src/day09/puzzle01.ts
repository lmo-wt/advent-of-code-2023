import { sum } from '../utils'
import parse from './parse'

export const extrapolate = (sequence: number[], reversed = false): number => {
  let allZeros = true
  const differences = []
  for (let i = 0; i < sequence.length - 1; i++) {
    const difference = sequence[i + 1] - sequence[i]
    differences.push(difference)
    if (difference !== 0) {
      allZeros = false
    }
  }
  if (allZeros) return reversed ? sequence[0] : sequence[sequence.length - 1]

  return reversed
    ? sequence[0] - extrapolate(differences, reversed) 
    : sequence[sequence.length - 1] + extrapolate(differences, reversed)
}

const solve = inputPath => {
  const sequences = parse(inputPath)
  const nexts = sequences.map(sequence => extrapolate(sequence))

  return sum(nexts)
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
