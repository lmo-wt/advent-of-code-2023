import parse from './parse'


export const rotatePattern = (pattern: string[]): string[] => {
  const rotated = new Array(pattern[0].length).fill(0).map((v, i) => pattern.map((line, j) => pattern[pattern.length - j - 1][i]).join(''))
  return rotated
}

export const isReflectionLine = (pattern: string[], index: number): boolean => {
  for (let j = 0; index - j >= 0 && index + j + 1 < pattern.length; j += 1) {
    if (pattern[index - j] !== pattern[index + j + 1]) {
      return false
    }
  }
  return true
}

export const getReflectionLine = (pattern: string[], ignoreReflectionLine: number = null): number => {
  for (let i = 0; i < pattern.length - 1; i += 1) {
    if (isReflectionLine(pattern, i) && i + 1 !== ignoreReflectionLine) {
      return i + 1
    }
  }
  return null
}

const solve = inputPath => {
  const patterns = parse(inputPath)
  let solution = 0
  for (const pattern of patterns) {
    const horizontalReflection = getReflectionLine(pattern)
    if (horizontalReflection) {
      solution += horizontalReflection * 100
    } else {
      const rotated = rotatePattern(pattern)
      const verticalReflection = getReflectionLine(rotated)
      if (verticalReflection) {
        solution += verticalReflection
      } else {
        throw new Error('No reflection found')
      }
    }
  }

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
