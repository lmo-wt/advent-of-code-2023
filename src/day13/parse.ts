import { chunkify, readlines } from '../utils'

const parse = (inputPath: string): string[][] => {
  const patterns = chunkify(readlines(inputPath, false), '')
  return patterns
}

export default parse
