import * as cliProgress from 'cli-progress'
import parse from './parse'
import { getReflectionLine, rotatePattern } from './puzzle01'

enum ReflectionLineType {
  Row = 'row',
  Column = 'column',
}

type ReflectionLine = {
  type: ReflectionLineType,
  index: number,
}

const getPatternReflectionLine = (pattern: string[], ignoreReflectionLine: ReflectionLine = null): ReflectionLine => {
  const horizontalReflection = getReflectionLine(pattern, ignoreReflectionLine?.type === ReflectionLineType.Row ? ignoreReflectionLine.index : null)
  if (horizontalReflection) {
    return { type: ReflectionLineType.Row, index: horizontalReflection }
  }
  const rotated = rotatePattern(pattern)
  const verticalReflection = getReflectionLine(rotated, ignoreReflectionLine?.type === ReflectionLineType.Column ? ignoreReflectionLine.index : null)
  if (verticalReflection) {
    return { type: ReflectionLineType.Column, index: verticalReflection }
  }
  return null
}

const getFixedPatternReflectionLine = (pattern: string[]): number => {
  const unfixedReflectionLine = getPatternReflectionLine(pattern)
  for (let y = 0; y < pattern.length; y += 1) {
    for (let x = 0; x < pattern[0].length; x += 1) {
      const fixedPattern = pattern.map((line, rowIndex) => {
        if (rowIndex === y) {
          return line.slice(0, x) + (pattern[y][x] === '#' ? '.' : '#') + line.slice(x + 1)
        } else {
          return line
        }
      })
      const reflectionLine = getPatternReflectionLine(fixedPattern, unfixedReflectionLine)
      if (reflectionLine) {
        return reflectionLine.type === ReflectionLineType.Row ? reflectionLine.index * 100 : reflectionLine.index
      }
    }
  }
  throw new Error('No reflection found')
}

const solve = inputPath => {
  const patterns = parse(inputPath)
  let solution = 0
  const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic)
  bar.start(patterns.length, 0)
  for (const [index, pattern] of patterns.entries()) {
    bar.update(index + 1)
    solution += getFixedPatternReflectionLine(pattern)
  }
  bar.stop()

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
