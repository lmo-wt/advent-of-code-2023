import { table } from 'table'

type PuzzleTest = {
  averageDuration: number,
  numberOfTests: number,
  day: number,
  puzzle: number,
  success: boolean,
  memoryUsage: number,
}

class JestReporter {
  onRunComplete(contexts, aggregatedResults) {
    const tests = new Map<number, Map<number, PuzzleTest>>()
    for (const testResult of aggregatedResults.testResults) {
      const match = /[./]*\/day(?<rawDay>\d+)\/__test__\/puzzle(?<rawPuzzle>\d+)\.test\.ts/.exec(testResult.testFilePath)
      if (match) {
        const { groups: { rawDay, rawPuzzle } } = match
        const results = testResult.testResults
        const day = parseInt(rawDay)
        const puzzle = parseInt(rawPuzzle)
        if (!tests.has(day)) {
          tests.set(day, new Map<number, PuzzleTest>())
        }
        tests.get(day).set(puzzle, {
          day,
          puzzle,
          success: results.every(result => result.status === 'passed'),
          numberOfTests: results.length,
          averageDuration: results.reduce((acc, result) => acc + result.duration, 0) / results.length,
          memoryUsage: testResult.memoryUsage / 1000000,
        })
      }
    }

    
    const elements = [...tests.keys()].map(day => {
      const test = tests.get(day)
      const puzzle01 = test.get(1)
      const puzzle02 = test.get(2)
      return [
        day.toString().padStart(2, '0'),
        puzzle01?.success ? '✓' : '✗',
        puzzle01 ? `${puzzle01.averageDuration.toFixed(1)} ms` : 'N/A',
        puzzle01 ? `${puzzle01.memoryUsage.toFixed(0)} MB` : 'N/A',
        puzzle01 ? puzzle01.numberOfTests.toString() : '0',
        puzzle02?.success ? '✓' : '✗',
        puzzle02 ? `${puzzle02.averageDuration.toFixed(1)} ms` : 'N/A',
        puzzle02 ? `${puzzle02.memoryUsage.toFixed(0)} MB` : 'N/A',
        puzzle02 ? puzzle02.numberOfTests.toString() : '0',
      ]
    }).sort((a, b) => parseInt(a[0]) - parseInt(b[0]))
    
    const summary: string[][] = [
      ['Day', 'Puzzle 01', '', '', '', 'Puzzle 02', '', '', ''],
      ['Day', 'Success', 'Avg time', 'Heap', 'Tests', 'Success', 'Avg time','Heap', 'Tests'],
      ...elements
    ]
    console.table(table(summary, {
      drawHorizontalLine: (lineIndex, rowCount) => {
        return lineIndex <= 2 || lineIndex === rowCount
      },
      columns: [{ alignment: 'center', width: 5 }, ...new Array(summary[0].length - 1).fill({ alignment: 'center' })],
      spanningCells: [
        { col: 1, row: 0, colSpan: 4 },
        { col: 5, row: 0, colSpan: 4 },
        { col: 0, row: 0, rowSpan: 2, verticalAlignment: 'middle'},
      ],
    }))
  }
}

module.exports = JestReporter