import { readlines } from '../utils'

export enum Condition {
  Operational = '.',
  Damaged = '#',
  Unknown = '?',
}

export type Record = {
  conditions: string,
  groups: number[],
}

const parse = (inputPath: string): Record[] => {
  const records = readlines(inputPath).map((line: string) => {
    const [conditions, rawGroups] = line.split(' ')
    return {
      groups: rawGroups.split(',').map(rawGroup => parseInt(rawGroup)),
      conditions,
    } as Record
  })

  return records
}

export default parse
