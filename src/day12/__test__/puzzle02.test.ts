import { guessRecordArrangements } from '../puzzle01'
import solve, { expandConditions, expandGroups } from '../puzzle02'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(525152)
})

test('guessRecordArrangements 1', () => {
  expect(guessRecordArrangements({ 
    conditions: '?????.??.???.??????.??.???.??????.??.???.??????.??.???.??????.??.???.',
    groups: [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
  })).toBeGreaterThan(0)
})

test('guessRecordArrangements 2', () => {
  expect(guessRecordArrangements({ 
    conditions: expandConditions('.??..??...?##.'),
    groups: expandGroups([1,1,3])
  })).toEqual(16384)
})

test('guessRecordArrangements 3', () => {
  expect(guessRecordArrangements({ 
    conditions: expandConditions('?#?#?#?#?#?#?#?'),
    groups: expandGroups([1,3,1,6])
  })).toEqual(1)
})

test('guessRecordArrangements 4', () => {
  expect(guessRecordArrangements({ 
    conditions: expandConditions('????.#...#...'),
    groups: expandGroups([4,1,1])
  })).toEqual(16)
})

test('guessRecordArrangements 5', () => {
  expect(guessRecordArrangements({ 
    conditions: expandConditions('????.######..#####.'),
    groups: expandGroups([1,6,5])
  })).toEqual(2500)
})

test('guessRecordArrangements 6', () => {
  expect(guessRecordArrangements({ 
    conditions: expandConditions('?###????????'),
    groups: expandGroups([3,2,1])
  })).toEqual(506250)
})
