import solve, { guessRecordArrangements, isArrangementValid } from '../puzzle01'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(21)
})

test('isArrangementValid', () => {
  expect(isArrangementValid('.###.##.#...', [3, 2, 1])).toEqual(true)
  expect(isArrangementValid('.???.##.#...', [3, 2, 1])).toEqual(true)
  expect(isArrangementValid('####.##.#...', [3, 2, 1])).toEqual(false)
})

test('guessRecordArrangements 1', () => {
  expect(guessRecordArrangements({ 
    conditions: '???.###',
    groups: [1, 1, 3]
  })).toEqual(1)
})

test('guessRecordArrangements 2', () => {
  expect(guessRecordArrangements({ 
    conditions: '.??..??...?##.',
    groups: [1, 1, 3]
  })).toEqual(4)
})

test('guessRecordArrangements 3', () => {
  expect(guessRecordArrangements({ 
    conditions: '?#?#?#?#?#?#?#?',
    groups: [1, 3, 1, 6]
  })).toEqual(1)
})

test('guessRecordArrangements 4', () => {
  expect(guessRecordArrangements({ 
    conditions: '????.#...#...',
    groups: [4, 1, 1]
  })).toEqual(1)
})

test('guessRecordArrangements 5', () => {
  expect(guessRecordArrangements({ 
    conditions: '????.######..#####.',
    groups: [1, 6, 5]
  })).toEqual(4)
})

test('guessRecordArrangements 6', () => {
  expect(guessRecordArrangements({ 
    conditions: '?###????????',
    groups: [3, 2, 1]
  })).toEqual(10)
})
