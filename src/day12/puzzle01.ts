import { sum } from '../utils'
import parse, { Record } from './parse'
import * as memoize from 'memoizee'

export const isArrangementValid = (arrangement: string, groups: number[]): boolean => {
  const completeRegex = `^[.?]*${groups.map(group => `[#?]{${group}}`).join('[.?]+')}[.?]*$`
  const regex = new RegExp(completeRegex)
  return regex.test(arrangement)
}

const _guessRecordArrangements = memoize((conditions: string, groups: string): number => {
  if (conditions.length === 0) {
    return groups.length === 0 ? 1 : 0
  }
  if (groups.length === 0) {
    return conditions.includes('#') ? 0 : 1
  }
  const actualGroups = groups.split(',').map(group => parseInt(group))
  const needed = sum(actualGroups)
  const minimum = conditions.match(/#/g)?.length || 0
  const maximum = conditions.match(/[?#]/g)?.length || 0
  if (needed < minimum || needed > maximum) {
    return 0
  } else if (conditions[0] === '.') {
    return _guessRecordArrangements(conditions.slice(1), groups)
  } else if (conditions[0] === '#') {
    const nextGroup = actualGroups[0]
    const followingOperationals = conditions.match(/^[#]+/g)[0]
    const followingPossibles = conditions.match(/^[?#]+/g)[0]
    if (followingPossibles.length >= nextGroup && followingOperationals.length <= nextGroup && conditions[nextGroup] !== '#') {
      return _guessRecordArrangements(conditions.slice(nextGroup + 1), actualGroups.slice(1).join(','))
    } else {
      return 0
    }
  } else { // conditions[0] === '?'
    return _guessRecordArrangements(conditions.slice(1), groups) + _guessRecordArrangements('#' + conditions.slice(1), groups)
  }
})

export const guessRecordArrangements = (record: Record): number => {
  const arrangements = _guessRecordArrangements(record.conditions, record.groups.join(','))
  return arrangements
}

const solve = inputPath => {
  const records = parse(inputPath)
  const solution = records.reduce((acc, record) => {
    const arrangements = guessRecordArrangements(record)
    return acc + arrangements
  }, 0)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
