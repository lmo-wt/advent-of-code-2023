import * as cliProgress from 'cli-progress'
import parse from './parse'
import { guessRecordArrangements } from './puzzle01'

export const expandConditions = (conditions: string): string => {
  return new Array(5).fill(0).map(() => conditions).join('?')
}

export const expandGroups = (groups: number[]): number[] => {
  return new Array(5).fill(0).map(() => groups).flat()
}

const solve = inputPath => {
  const records = parse(inputPath).map(record => ({
    conditions: expandConditions(record.conditions),
    groups: expandGroups(record.groups),
  }))

  const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic)
  bar.start(records.length, 0)
  const solution = records.reduce((acc, record, index) => {
    bar.update(index + 1)
    const arrangements = guessRecordArrangements(record)
    return acc + arrangements
  }, 0)
  bar.stop()
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
