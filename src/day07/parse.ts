import { readlines } from '../utils'

export type Hand = {
  cards: string[],
  bid: number,
}

export enum HandType {
  FiveOfaKind = 7,
  FourOfaKind = 6,
  FullHouse = 5,
  ThreeOfaKind = 4,
  TwoPairs = 3,
  OnePair = 2,
  HighCard = 1,
}

export const STRENGTHS = {
  'A': 12,
  'K': 11,
  'Q': 10,
  'J': 9,
  'T': 8,
  '9': 7,
  '8': 6,
  '7': 5,
  '6': 4,
  '5': 3,
  '4': 2,
  '3': 1,
  '2': 0,
}

const parse = (inputPath: string): Hand[] => {
  const hands = readlines(inputPath).map((line: string) => {
    const [rawCards, rawBid] = line.split(' ')
    const cards = rawCards.split('')
    const bid = parseInt(rawBid)
    
    return { cards, bid } as Hand
  })

  return hands
}

export default parse
