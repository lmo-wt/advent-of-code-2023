import parse, { HandType, Hand as RawHand} from './parse'

type Hand = RawHand & {
  strengths: number[],
  type: number,
}

export const STRENGTHS = {
  'A': 12,
  'K': 11,
  'Q': 10,
  'T': 9,
  '9': 8,
  '8': 7,
  '7': 6,
  '6': 5,
  '5': 4,
  '4': 3,
  '3': 2,
  '2': 1,
  'J': 0,
}

const solve = inputPath => {
  const rawHands = parse(inputPath)
  const hands: Hand[] = rawHands.map((hand: RawHand) => {
    const strengths = new Array(13).fill(0)
    for (const card of hand.cards) {
      strengths[STRENGTHS[card]] += 1
    }
    const jokers = strengths.shift()
    const orderedStrengths = strengths.sort((a, b) => b - a)
    let type = HandType.HighCard
    if (orderedStrengths[0] + jokers === 5) {
      type = HandType.FiveOfaKind
    } else if (orderedStrengths[0] + jokers === 4) {
      type = HandType.FourOfaKind
    } else if (orderedStrengths[0] + jokers === 3 && orderedStrengths[1] === 2) {
      type = HandType.FullHouse
    } else if (orderedStrengths[0] + jokers === 3) {
      type = HandType.ThreeOfaKind
    } else if (orderedStrengths[0] + jokers === 2 && orderedStrengths[1] === 2) {
      type = HandType.TwoPairs
    } else if (orderedStrengths[0] + jokers === 2) {
      type = HandType.OnePair
    }
    return { ...hand, strengths, type } as Hand
  })

  const orderedHands = hands.sort((a, b) => {
    if (a.type !== b.type) {
      return b.type - a.type
    } else {
      for (let i = 0; i < a.cards.length; i++) {
        if (a.cards[i] !== b.cards[i]) {
          return STRENGTHS[b.cards[i]] - STRENGTHS[a.cards[i]]
        }
      }
      return 0
    }
  })

  const solution = orderedHands.reduce((acc, hand, index) => {
    return acc + (hand.bid * (orderedHands.length - index))
  }, 0)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
