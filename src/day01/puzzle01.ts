import { sum } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const parsed = parse(inputPath).map(digits => digits.split(''))
  const calibrationValues = parsed.map(digits => {
    const first = digits.find(digit => '0123456789'.includes(digit))
    const last = digits.findLast(digit => '0123456789'.includes(digit))
    return parseInt(`${first}${last}`)
  })

  return sum(calibrationValues)
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
