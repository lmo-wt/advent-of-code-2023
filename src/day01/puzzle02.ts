import { sum } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const lines = parse(inputPath)
  const digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
  const calibrationValues = lines.map((line: string) => {
    const find = (reverse = false) => {
      for (let i = 0; i < line.length; i += 1) {
        const substring = reverse ? line.slice(0, line.length - i) : line.slice(i)
        for (let n = 0; n < digits.length; n += 1) {
          if (substring[reverse ? 'endsWith' : 'startsWith'](`${n}`) || substring[reverse ? 'endsWith' : 'startsWith'](digits[n])) {
            return n
          }
        }
      }
      throw new Error(`No ${reverse ? 'last' : 'first'} digit found in ${line}`)
    }
    
    const first = find()
    const last = find(true)
    return parseInt(`${first}${last}`)
  })

  return sum(calibrationValues)
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
