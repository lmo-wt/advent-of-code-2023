import { readlines } from '../utils'

const parse = (inputPath: string): string[] => {
  const parsed = readlines(inputPath)

  return parsed
}

export default parse
