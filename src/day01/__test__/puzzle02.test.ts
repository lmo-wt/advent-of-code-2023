import solve from '../puzzle02'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input02.txt`)
  expect(solution).toEqual(281)
})
