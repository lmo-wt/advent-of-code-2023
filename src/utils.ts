import dayjs = require('dayjs')
import { readFileSync, existsSync } from 'fs'
import duration = require('dayjs/plugin/duration')
import relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(duration)
dayjs.extend(relativeTime)

export const readlines = (path, filterEmptyLines = true, trim = true): string[] => {
  if (!existsSync(path)) throw new Error(`File does not exists: ${path}`)
  return readFileSync(path, 'utf-8').split('\n')
    .map(line => trim ? line.trim() : line)
    .filter(line => filterEmptyLines === false || line.length > 0)
}

export const objectAndArrayFunction = (objectBehaviour, arrayBehaviour) => {
  return object => {
    if (Array.isArray(object)) {
      return arrayBehaviour(object)
    } else {
      return objectBehaviour(object)
    }
  }
}

export const forIn = (o, f) => {
  for (const key of Object.keys(o)) {
    f(key, o[key])
  }
}

export const every = (collection, predicate) => {
  return objectAndArrayFunction(
    object => {
      for (const key of Object.keys(object)) {
        if (!predicate(object[key])) {
          return false
        }
      }
      return true
    },
    array => {
      for (const element of array) {
        if (!predicate(element)) {
          return false
        }
      }
      return true
    }
  )(collection)
}

export const filterObject = (o, predicate) => {
  const filtered = {}
  forIn(o, (key, value) => {
    if (predicate(key, value)) {
      filtered[key] = value
    }
  })
  return filtered
}

export const slugify = text => text.toString().toLowerCase().trim()
  .replace(/&/g, '-and-')         // Replace & with 'and'
  .replace(/[\s\W-]+/g, '-') 

export const cleanDuplicates = array => {
  if (array.length === 0) return array
  const cleaned = [array[0]]
  for (const element of array.slice(1)) {
    if (!cleaned.includes(element)) {
      cleaned.push(element)
    }
  }
  return cleaned
}

export const sum = (array, getter = v => v) => array.reduce((acc, value) => acc + getter(value), 0)
export const min = (array, getter = v => v) => array.reduce((a, b) => getter(b) < getter(a) ? b : a, array[0])
export const max = (array, getter = v => v) => array.reduce((a, b) => getter(b) > getter(a) ? b : a, array[0])

export const intersection = (...arrays) => {
  if (arrays.length === 1) {
    return arrays[0]
  } else if (arrays.length === 2) {
    return arrays[0].filter(element => arrays[1].includes(element))
  } else {
    const [first, second, ...others] = arrays
    return intersection(intersection(first, second), ...others)
  }
}

export const binToDec = binary => binary
  .reverse()
  .reduce((previous, current, index) => previous + (current ? Math.pow(2, index) : 0), 0)

export const isFunction = (functionToCheck) => functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'

export const chunkify = (collection, separator) => {
  const chunks = []
  let chunk = []
  const checkElement = element => isFunction(separator) ? separator(element) : element === separator
  for (const element of collection) {
    if (checkElement(element)) {
      chunks.push([...chunk])
      chunk = []
    } else {
      chunk.push(element)
    }
  }
  if (chunk.length > 0) {
    chunks.push([...chunk])
  }
  return chunks
}

export const samples = (collection: unknown[], size: number) => {
  const result = []
  for (let n = 0; n < collection.length; n += size) {
    result.push(collection.slice(n, n + size))
  }
  return result
}

type displayValueFunction<T> = (value: T, x?: number, y?: number, grid?: Grid<T>) => string
export class Grid<T> {
  values: T[][]
  sizex: number
  sizey: number
  xmax: number
  ymax: number
  negativeCoordinates: boolean
  displayValue: displayValueFunction<T> = (value: T) => value as string

  actualX (x: number): number {
    return this.negativeCoordinates ? ((this.sizex + 1) / 2) - 1 + x : x
  }
  actualY (y: number): number {
    return this.negativeCoordinates ? ((this.sizey + 1) / 2) - 1 - y : this.sizey - 1 - y
  }
  get (x: number, y: number): T {
    return this.values[this.actualY(y)][this.actualX(x)]
  }
  set (x: number, y: number, value: Partial<T> | ((previous: T) => T)): T {
    if (typeof value === 'function') {
      this.values[this.actualY(y)][this.actualX(x)] = (value as ((previous: T) => T))(this.get(x, y))
    } else if (typeof value === 'object' && !Array.isArray(value)) {
      this.values[this.actualY(y)][this.actualX(x)] = { ...this.get(x, y), ...value }
    } else {
      this.values[this.actualY(y)][this.actualX(x)] = value as T
    }
    return this.get(x, y)
  }
  row (y: number): T[] {
    return this.values[this.actualY(y)]
  }
  col (x: number): T[] { 
    return this.values.map(row => row[this.actualX(x)]).reverse()
  }
  display () {
    return this.values.map((row, y) => row.map((value, x) => this.displayValue(value, x, y)).join(''))
  }
  console (wait = 0): void {
    console.clear()
    console.log(this.display().join('\n'))
    if (wait > 0) {
      sleep(wait)
    }
  }
  iterate (callback: (value: T, x: number, y: number, grid: Grid<T>) => void): void {
    for (let y = 0; y <= this.ymax; y++) {
      for (let x = 0; x <= this.xmax; x++) {
        callback(this.get(x, y), x, y, this)
      }
    }
  }
  isOOB (x: number, y: number): boolean {
    return x < 0 || x > this.xmax || y < 0 || y > this.ymax
  }
}

export const makeGrid = <T>(values: T[][], negativeCoordinates = false, displayValue: displayValueFunction<T> = ((value: T) => value as string)): Grid<T> => {
  const grid = new Grid<T>()
  grid.values = values
  grid.negativeCoordinates = negativeCoordinates
  grid.displayValue = displayValue
  grid.sizex = values[0].length
  grid.sizey = values.length
  grid.xmax = negativeCoordinates ? (grid.sizex - 1) / 2 : grid.sizex - 1
  grid.ymax = negativeCoordinates ? (grid.sizey - 1) / 2 : grid.sizey - 1
  
  return grid
}

export const generateGrid = <T>(sizex: number, sizey: number, initValue: T, negativeCoordinates = false, displayValue: displayValueFunction<T> = ((value: T) => value as string)): Grid<T> => {
  const values = new Array(negativeCoordinates ? sizey * 2 - 1 : sizey).fill(0).map(() => new Array(negativeCoordinates ? sizex * 2 - 1 : sizex).fill(0).map(() => initValue))
  return makeGrid(values, negativeCoordinates, displayValue)
}

class BidimensionalObject {
  x: number
  y: number

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  hash (): string {
    return `x${this.x}y${this.y}`
  }

  equals (point: Point): boolean {
    return this.x === point.x && this.y === point.y
  }
}

export class Point extends BidimensionalObject {
  translate (vector: Vector): Point {
    return new Point(this.x + vector.x, this.y + vector.y)
  }
}

export class Vector extends BidimensionalObject {
  multiply (factor: number): Vector {
    return new Vector(this.x * factor, this.y * factor)
  }
}

/* istanbul ignore next */
export const sleep = (n: number): void => {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n)
}

export const SORT_DESC = (a, b) => a > b ? -1 : 1
export const SORT_ASC = (a, b) => a > b ? 1 : -1

export const manhattan = (a: Point, b: Point) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y)

export function* combinationGenerator<T>(arr: T[]) {
  if (arr.length <= 1) {
    yield arr
  } else {
    for (const [index, item] of arr.entries()) {
      const subarr = [...arr.slice(0, index), ...arr.slice(index + 1)]
      const gen = combinationGenerator(subarr)
      let combination = gen.next()
      do {
        yield [item, ...combination.value]
        combination = gen.next()
      } while (!combination.done)
    }
  }
  return
}

export const factorial = (n: number): number => {
  if (n <= 0) {
    return 0
  } else if (n == 1) {
    return 1
  } else {
    return n * factorial(n - 1)
  }
}

const termialMemo = new Map<number, number>()
export const termial = (n: number): number => {
  if (n <= 0) {
    return 0
  } else if (n == 1) {
    return 1
  } else if (termialMemo.has(n)) {
    return termialMemo.get(n)
  } else {
    const result = n + termial(n - 1)
    termialMemo.set(n, result)
    return result
  }
}

/* istanbul ignore next */
export const createTimer = () => {
  const start = dayjs()
  const elapsed = () => dayjs.duration(dayjs().diff(start))
  return {
    elapsed,
    remaining: (current: number): duration.Duration => {
      const currentElapsed = elapsed()
      return dayjs.duration(currentElapsed.asMilliseconds() / current, 'ms').subtract(currentElapsed)
    }
  }
}

export const mapObject = <T, V>(o: {[key: string]: V}, mapper: (value: V, key: string) => T): {[key: string]: T} => {
  const result: {[key: string]: T} = {}
  for (const key in o) {
    result[key] = mapper(o[key], key)
  }
  return result
}

const _gcd = (a: number, b: number): number => {
  if (b === 0) {
    return a
  }
  return _gcd(b, a % b)
}
export const gcd = (...numbers: number[]): number => {
  return numbers.reduce((a, b) => _gcd(a, b))
}

const _lcm = (a: number, b: number): number => {
  return (a * b) / _gcd(a, b)
}
export const lcm = (...numbers: number[]): number => {
  return numbers.reduce((a, b) => _lcm(a, b))
}

export const shoelace = (...vertices: Point[]): number => {
  let area = 0
  for (let i = 0; i < vertices.length; i++) {
    const a = vertices[i]
    const b = vertices[i === vertices.length - 1 ? 0 : i + 1]
    area += a.x * b.y - a.y * b.x
  }
  area /= 2
  return Math.abs(area)
}

export type GraphEdge<V> = {
  node: GraphNode<V>
  weight: number
}

export type GraphNode<V> = {
  value: V
  cost: number
  edges: GraphEdge<V>[]
}

export class Graph<V> {
  nodes: Map<string, GraphNode<V>> = new Map()

  addNode (id: string, value: V): GraphNode<V> {
    const node = { value, cost: Infinity, edges: [] }
    this.nodes.set(id, node)
    return node
  }

  dijkstra (origin: GraphNode<V>) {
    for (const node of this.nodes.values()) {
      node.cost = Infinity
    }
    origin.cost = 0
    this._dijkstra(origin)
  }

  _dijkstra (node: GraphNode<V>) {
    for (const edge of node.edges) {
      const neighbour = edge.node
      const cost = node.cost + edge.weight
      if (cost < neighbour.cost) {
        neighbour.cost = cost
        this._dijkstra(neighbour)
      }
    }
  }
}
