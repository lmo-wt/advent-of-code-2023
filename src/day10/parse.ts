import { Grid, Point, makeGrid, readlines } from '../utils'

export enum Tile {
  Ground = '.',
  BottomRightCorner = 'F',
  BottomLeftCorner = '7',
  TopLeftCorner = 'J',
  TopRightCorner = 'L',
  Vertical = '|',
  Horizontal = '-',
}

const parse = (inputPath: string): { startPosition: Point, grid: Grid<Tile> } => {
  const startPosition = new Point(0, 0)
  const values = readlines(inputPath).map((line: string) => line.split(''))
  const tiles = values.map((row: string[], y: number) => row.map((char: string, x: number) => {
    if (char === 'S') {
      startPosition.x = x
      startPosition.y = values.length - 1 - y
      const top = y === 0 ? null : values[y - 1][x] as Tile
      const bottom = y === row.length - 1 ? null : values[y + 1][x] as Tile
      const left = x === 0 ? null : values[y][x - 1] as Tile
      const right = x === row.length - 1 ? null : values[y][x + 1] as Tile
      const topConnected = [Tile.BottomLeftCorner, Tile.BottomRightCorner, Tile.Vertical].includes(top)
      const bottomConnected = [Tile.TopLeftCorner, Tile.TopRightCorner, Tile.Vertical].includes(bottom)
      const leftConnected = [Tile.TopRightCorner, Tile.BottomRightCorner, Tile.Horizontal].includes(left)
      const rightConnected = [Tile.TopLeftCorner, Tile.BottomLeftCorner, Tile.Horizontal].includes(right)
      if (topConnected && bottomConnected) return Tile.Vertical
      if (topConnected && leftConnected) return Tile.TopLeftCorner
      if (topConnected && rightConnected) return Tile.TopRightCorner
      if (bottomConnected && leftConnected) return Tile.BottomLeftCorner
      if (bottomConnected && rightConnected) return Tile.BottomRightCorner
      if (leftConnected && rightConnected) return Tile.Horizontal
      else throw new Error(`Invalid start position at ${x}, ${y}`)
    } else {
      return char as Tile
    }
  }))

  const grid = makeGrid(tiles)

  return { startPosition, grid }
}

export default parse
