import { shoelace } from '../utils'
import parse from './parse'
import { computeLoop } from './puzzle01'

const solve = inputPath => {
  const { grid, startPosition } = parse(inputPath)
  const loop = computeLoop(grid, startPosition)

  const area = shoelace(...loop)
  const solution = area - (loop.length / 2) + 1

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
