import { Grid, Point } from '../utils'
import parse, { Tile } from './parse'

export const computeLoop = (grid: Grid<Tile>, startPosition: Point) => {
  const loop: Point[] = []
  let previousPosition: Point = null
  let currentPosition = startPosition
  do {
    loop.push(currentPosition)
    const nextPositions = []
    const tile = grid.get(currentPosition.x, currentPosition.y)
    if (tile === Tile.BottomLeftCorner) {
      nextPositions.push(new Point(currentPosition.x - 1, currentPosition.y))
      nextPositions.push(new Point(currentPosition.x, currentPosition.y - 1))
    } else if (tile === Tile.BottomRightCorner) {
      nextPositions.push(new Point(currentPosition.x + 1, currentPosition.y))
      nextPositions.push(new Point(currentPosition.x, currentPosition.y - 1))
    } else if (tile === Tile.TopLeftCorner) {
      nextPositions.push(new Point(currentPosition.x - 1, currentPosition.y))
      nextPositions.push(new Point(currentPosition.x, currentPosition.y + 1))
    } else if (tile === Tile.TopRightCorner) {
      nextPositions.push(new Point(currentPosition.x + 1, currentPosition.y))
      nextPositions.push(new Point(currentPosition.x, currentPosition.y + 1))
    } else if (tile === Tile.Horizontal) {
      nextPositions.push(new Point(currentPosition.x + 1, currentPosition.y))
      nextPositions.push(new Point(currentPosition.x - 1, currentPosition.y))
    } else if (tile === Tile.Vertical) {
      nextPositions.push(new Point(currentPosition.x, currentPosition.y + 1))
      nextPositions.push(new Point(currentPosition.x, currentPosition.y - 1))
    }
    const possibleNextPositions = nextPositions
      .filter(position => loop.length === 1 || !position.equals(previousPosition))
      .filter(position => position.x >= 0 && position.x <= grid.xmax && position.y >= 0 && position.y <= grid.ymax)
    previousPosition = currentPosition
    currentPosition = possibleNextPositions[0]
  } while (!currentPosition.equals(startPosition))
  return loop
}

const solve = inputPath => {
  const { grid, startPosition } = parse(inputPath)
  const loop = computeLoop(grid, startPosition)
  const solution = loop.length / 2
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
