import solve from '../puzzle02'

test('solves test input03', () => {
  const solution = solve(`${__dirname}/input03.txt`)
  expect(solution).toEqual(4)
})

test('solves test input04', () => {
  const solution = solve(`${__dirname}/input04.txt`)
  expect(solution).toEqual(8)
})
