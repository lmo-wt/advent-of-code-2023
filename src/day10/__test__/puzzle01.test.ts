import solve from '../puzzle01'

test('solves test input01', () => {
  const solution = solve(`${__dirname}/input01.txt`)
  expect(solution).toEqual(4)
})

test('solves test input02', () => {
  const solution = solve(`${__dirname}/input02.txt`)
  expect(solution).toEqual(8)
})
