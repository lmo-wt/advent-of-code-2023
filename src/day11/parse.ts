import { readlines, Point } from '../utils'

const parse = (inputPath: string): Point[] => {
  const values = readlines(inputPath).map((line: string) => line.split(''))
  const xmax = values[0].length - 1
  const ymax = values.length - 1
  const galaxies = []
  for (let y = 0; y <= ymax; y += 1) {
    for (let x = 0; x <= xmax; x += 1) {
      if (values[y][x] === '#') {
        galaxies.push(new Point(x, y))
      }
    }
  }

  return galaxies
}

export default parse
