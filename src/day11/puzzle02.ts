import { solve as solve01 } from './puzzle01'

export const solve = (inputPath: string, expandingFactor = 1000000) => {
  const solution = solve01(inputPath, expandingFactor)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
