import { manhattan } from '../utils'
import parse from './parse'

export const solve = (inputPath: string, expandingFactor = 2) => {
  const galaxies = parse(inputPath)

  const xmax = Math.max(...galaxies.map(galaxy => galaxy.x))
  const ymax = Math.max(...galaxies.map(galaxy => galaxy.y))
  const emptyRows = []
  for (let y = 0; y <= ymax; y += 1) {
    if (!galaxies.find(galaxy => galaxy.y === y)) {
      emptyRows.push(y)
    }
  }
  const emptyColumns = []
  for (let x = 0; x <= xmax; x += 1) {
    if (!galaxies.find(galaxy => galaxy.x === x)) {
      emptyColumns.push(x)
    }
  }
  for (let i = 0; i < emptyRows.length; i += 1) {
    for (const galaxy of galaxies) {
      if (galaxy.y > emptyRows[i]) {
        galaxy.y += expandingFactor - 1
      }
    }
    for (let j = i + 1; j < emptyRows.length; j += 1) {
      emptyRows[j] += expandingFactor - 1
    }
  }
  for (let i = 0; i < emptyColumns.length; i += 1) {
    for (const galaxy of galaxies) {
      if (galaxy.x > emptyColumns[i]) {
        galaxy.x += expandingFactor - 1
      }
    }
    for (let j = i + 1; j < emptyColumns.length; j += 1) {
      emptyColumns[j] += expandingFactor - 1
    }
  }

  let solution = 0
  for (let i = 0; i < galaxies.length; i += 1) {
    for (let j = i + 1; j < galaxies.length; j += 1) {
      const distance = manhattan(galaxies[i], galaxies[j])
      solution += distance
    }
  }
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
