import solve from '../puzzle02'

test('solves test input - 10 factor', () => {
  const solution = solve(`${__dirname}/input.txt`, 10)
  expect(solution).toEqual(1030)
})

test('solves test input - 100 factor', () => {
  const solution = solve(`${__dirname}/input.txt`, 100)
  expect(solution).toEqual(8410)
})
