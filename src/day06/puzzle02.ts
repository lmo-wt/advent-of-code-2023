import parse from './parse02'
import { solveRace } from './puzzle01'

const solve = inputPath => {
  const race = parse(inputPath)
  const winningRange = solveRace(race)
  const solution = winningRange.max - winningRange.min + 1

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
