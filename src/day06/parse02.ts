import { readlines } from '../utils'
import { Race } from './parse01'

const parse = (inputPath: string): Race => {
  const [time, distance] = readlines(inputPath).map((line: string) => parseInt(line.replaceAll(/\s+/g, '').split(':')[1]))


  return { time, distance } as Race
}

export default parse
