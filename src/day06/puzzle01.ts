import parse, { Race } from './parse01'

export type Range = {
  min: number,
  max: number,
}

const isInt = (n: number) => n % 1 === 0
export const solveRace = (race: Race): Range => {
  const d = (race.time * race.time) - (4 * race.distance)
  const x1 = (race.time - Math.sqrt(d)) / 2
  const x2 = (race.time + Math.sqrt(d)) / 2

  return {
    min: isInt(x1) ? x1 + 1 : Math.ceil(x1),
    max: isInt(x2) ? x2 - 1 : Math.floor(x2),
  }
}

const solve = inputPath => {
  const races = parse(inputPath)
  const winningRanges = races.map(race => solveRace(race))

  const ways = winningRanges.map(winningRange => winningRange.max - winningRange.min + 1)
  const solution = ways.reduce((acc, way) => acc * way, 1)
 
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
