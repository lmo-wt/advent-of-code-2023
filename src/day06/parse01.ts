import { readlines } from '../utils'

export type Race = {
  time: number,
  distance: number,
}

const parse = (inputPath: string): Race[] => {
  const lines = readlines(inputPath).map((line: string) => line.replaceAll(/\s+/g, ' ').split(' ').map((x: string) => parseInt(x)))
  const times = lines[0].slice(1)
  const distances = lines[1].slice(1)

  const races = times.map((time: number, index: number) => ({ time, distance: distances[index] } as Race))

  return races
}

export default parse
