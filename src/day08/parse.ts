import { readlines } from '../utils'

export enum Direction {
  Left = 'L',
  Right = 'R',
}

export type Node = {
  id: string
  left: string
  right: string
  stepsToEndNodes: Map<string, number>
}

const parse = (inputPath: string): { 
  directions: Direction[],
  nodes: Map<string, Node>
} => {
  const [rawDirections, ...rawNodes] = readlines(inputPath)
  const directions = rawDirections.split('') as Direction[]
  const nodes = new Map<string, Node>()
  for (const rawNode of rawNodes) {
    const { groups: { id, left, right } } = /(?<id>[A-Z0-9]+) = \((?<left>[A-Z0-9]+), (?<right>[A-Z0-9]+)\)/.exec(rawNode)
    nodes.set(id, { id, left, right, stepsToEndNodes: new Map<string, number>() })
  }

  return { directions, nodes }
}

export default parse
