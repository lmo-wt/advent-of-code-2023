import parse, { Direction } from './parse'

const solve = inputPath => {
  const { directions, nodes } = parse(inputPath)
  let steps = 0
  let currentNode = 'AAA'
  while (currentNode !== 'ZZZ') {
    const nextDirection = directions[steps % directions.length]
    currentNode = nodes.get(currentNode)[nextDirection === Direction.Left ? 'left' : 'right']
    steps += 1
  }

  return steps
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
