import { lcm } from '../utils'
import parse, { Direction } from './parse'

const solve = inputPath => {
  const { directions, nodes } = parse(inputPath)
  const startingNodes = [...nodes.keys()].filter(node => node.endsWith('A'))
  const stepsPerNode = startingNodes.map((startingNode) => {
    let steps = 0
    let currentNode = startingNode
    while (!currentNode.endsWith('Z')) {
      const nextDirection = directions[steps % directions.length]
      currentNode = nodes.get(currentNode)[nextDirection === Direction.Left ? 'left' : 'right']
      steps += 1
    }
    return steps
  })
  const solution = lcm(...stepsPerNode)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
