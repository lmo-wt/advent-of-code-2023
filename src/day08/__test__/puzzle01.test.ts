import solve from '../puzzle01'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input01.txt`)
  expect(solution).toEqual(6)
})
