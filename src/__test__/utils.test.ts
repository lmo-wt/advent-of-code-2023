import { 
  readlines,
  objectAndArrayFunction,
  every,
  forIn,
  filterObject,
  slugify,
  cleanDuplicates,
  sum,
  min,
  max,
  intersection,
  binToDec,
  chunkify,
  samples,
  generateGrid,
  makeGrid,
  SORT_ASC,
  SORT_DESC,
  manhattan,
  combinationGenerator,
  factorial,
  termial,
  gcd,
  lcm,
  Point,
  Vector,
  shoelace,
} from '../utils'

test('readlines', () => {
  expect(readlines(`${__dirname}/__data__/input.txt`)).toEqual(['a b c', 'a', '123'])
})

test('objectAndArrayFunction', () => {
  const commonFunction = objectAndArrayFunction(() => 'object', () => 'array')
  expect(commonFunction({})).toEqual('object')
  expect(commonFunction([])).toEqual('array')

  const commonFunctionWithArgument = objectAndArrayFunction(o => Object.keys(o), a => a.length)
  expect(commonFunctionWithArgument({ foo: 'foo', bar: 'bar' })).toEqual(['foo', 'bar'])
  expect(commonFunctionWithArgument(['foo', 'bar'])).toEqual(2)
})

test('forIn', () => {
  const mockFn = jest.fn()
  forIn({ foo: 'bar', bar: 'baz'}, (key, value) => mockFn(key, value))
  expect(mockFn).toHaveBeenNthCalledWith(1, 'foo', 'bar')
  expect(mockFn).toHaveBeenNthCalledWith(2, 'bar', 'baz')
})

test('every', () => {
  expect(every([1, 2, 3], value => value > 0)).toEqual(true)
  expect(every([1, 2, 3], value => value < 0)).toEqual(false)
  expect(every([1, 2, 3], value => value > 1)).toEqual(false)

  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 0)).toEqual(true)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value < 0)).toEqual(false)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 1)).toEqual(false)
})

test('filterObject', () => {
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 0 && key !== 'baz')).toEqual({ foo: 1, bar: 2})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (_, value) => value < 0)).toEqual({})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 1 && key)).toEqual({ bar: 2, baz: 3})
})

test('slugify', () => {
  expect(slugify('Mdr le slu & gify    ')).toEqual('mdr-le-slu-and-gify')
})

test('cleanDuplicates', () => {
  expect(cleanDuplicates([1, 2, 1, 2, 3])).toEqual([1, 2, 3])
  expect(cleanDuplicates([])).toEqual([])
})

test('sum', () => {
  expect(sum([1, 2, 3])).toEqual(6)
})

test('sum - with getter', () => {
  expect(sum([{ v: 1 }, { v: 2 }, { v: 3 }], v => v.v)).toEqual(6)
})

test('min', () => {
  expect(min([1, 2, 3])).toEqual(1)
})

test('min - reversed order', () => {
  expect(min([{ a: 1 }, { a: 2 }, { a: 3 }], item => item.a)).toEqual({ a: 1 })
})

test('min - with getter', () => {
  expect(min([1, 2, 3])).toEqual(1)
})

test('min - with getter - reversed order', () => {
  expect(min([3, 2, 1])).toEqual(1)
  expect(min([{ a: 3 }, { a: 2 }, { a: 1 }], item => item.a)).toEqual({ a: 1 })
})

test('max', () => {
  expect(max([1, 2, 3])).toEqual(3)
})

test('max - reversed order', () => {
  expect(max([3, 2, 1])).toEqual(3)
})

test('max - with getter', () => {
  expect(max([{ a: 1 }, { a: 2 }, { a: 3 }], item => item.a)).toEqual({ a: 3 })
})

test('max - with getter - reversed order', () => {
  expect(max([{ a: 3 }, { a: 2 }, { a: 1 }], item => item.a)).toEqual({ a: 3 })
})

test('intersection', () => {
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5])).toEqual([1, 2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8])).toEqual([])
  expect(intersection([1, 2, 3, 4])).toEqual([1, 2, 3, 4])
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5], [2, 3, 4, 5])).toEqual([2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12])).toEqual([])
})


test('binToDec', () => {
  expect(binToDec([1, 0, 1, 1, 0])).toEqual(22)
})


test('chunkify', () => {
  expect(chunkify([1, null, 3, 4], null)).toEqual([[1], [3, 4]])
  expect(chunkify(['foo', 'bar', '--------', 'baz'], element => element.length > 3)).toEqual([['foo', 'bar'], ['baz']])
})

test('samples', () => {
  expect(samples([1, 2, 3, 4, 5, 6, 7, 8], 2)).toEqual([[1, 2], [3, 4], [5, 6], [7, 8]])
  expect(samples([1, 2, 3, 4, 5, 6], 3)).toEqual([[1, 2, 3], [4, 5, 6]])
  expect(samples([1, 2, 3, 4], 3)).toEqual([[1, 2, 3], [4]])
})

test('makeGrid', () => {
  const grid = makeGrid([[1, 2, 3, 0], [4, 5, 6, 0], [7, 8, 9, 0]])
  expect(grid.values).toEqual([[1, 2, 3, 0], [4, 5, 6, 0], [7, 8, 9, 0]])
  expect(grid.row(1)).toEqual([4, 5, 6, 0])
  expect(grid.col(2)).toEqual([9, 6, 3])
  expect(grid.sizex).toEqual(4)
  expect(grid.sizey).toEqual(3)

  expect(grid.get(2, 1)).toEqual(6)
  expect(grid.set(2, 1, 12)).toEqual(12)
  expect(grid.get(2, 1)).toEqual(12)
})

test('generateGrid', () => {
  const grid = generateGrid(3, 3, 0)

  expect(grid.values).toEqual([
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
  ])

  expect(grid.get(1, 1)).toEqual(0)
  expect(grid.set(1, 1, 12)).toEqual(12)
  expect(grid.get(1, 1)).toEqual(12)
})

test('generateGrid - negative coordinates', () => {
  const grid = generateGrid(3, 3, 0, true)

  expect(grid.values).toEqual([
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
  ])

  expect(grid.xmax).toEqual(2)
  expect(grid.ymax).toEqual(2)

  expect(grid.get(1, 1)).toEqual(0)
  expect(grid.set(1, 1, 12)).toEqual(12)
  expect(grid.get(1, 1)).toEqual(12)

  expect(grid.get(-1, 2)).toEqual(0)
  expect(grid.set(-1, 2, 24)).toEqual(24)
  expect(grid.get(-1, 2)).toEqual(24)
  expect(grid.set(-1, 2, previous => -previous)).toEqual(-24)
  expect(grid.get(-1, 2)).toEqual(-24)

  expect(grid.values).toEqual([
    [0, -24, 0, 0, 0],
    [0, 0, 0, 12, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
  ])
})

test('generateGrid - with objects', () => {
  const grid = generateGrid(3, 3, { a: 'foo', b: 'bar' }, true)
  expect(grid.get(1, 1)).toEqual({ a: 'foo', b: 'bar' })
  expect(grid.set(1, 1, { a: 'bar', b: 'baz' })).toEqual({ a: 'bar', b: 'baz' })
  expect(grid.get(1, 1)).toEqual({ a: 'bar', b: 'baz' })
  expect(grid.set(1, 1, { a: 'bazbaz', b: 'baz' })).toEqual({ a: 'bazbaz', b: 'baz' })
  expect(grid.get(1, 1)).toEqual({ a: 'bazbaz', b: 'baz' })

})

test('point translation', () => {
  let point = new Point(2, 4)
  point = point.translate(new Vector(5, 5))
  expect(point).toEqual({ x: 7, y: 9 })
  point = point.translate(new Vector(-10, 5))
  expect(point).toEqual({ x: -3, y: 14 })
})

test('point hash', () => {
  expect((new Point(0, 0)).hash()).toEqual('x0y0')
  expect((new Point(0, -1)).hash()).toEqual('x0y-1')
  expect((new Point(-1, 0)).hash()).toEqual('x-1y0')
  expect((new Point(-1, -1)).hash()).toEqual('x-1y-1')
})

test('sort asc', () => {
  expect([1, 2, 1, 2, 3].sort(SORT_ASC)).toEqual([1, 1, 2, 2, 3])
  expect([3, 1, 2, 2, 3].sort(SORT_ASC)).toEqual([1, 2, 2, 3, 3])
})

test('sort desc', () => {
  expect([1, 2, 1, 2, 3].sort(SORT_DESC)).toEqual([3, 2, 2, 1, 1])
  expect([3, 1, 2, 2, 3].sort(SORT_DESC)).toEqual([3, 3, 2, 2, 1])
})

test('sort manhattan', () => {
  expect(manhattan(new Point(0, 0), new Point(0, 0))).toEqual(0)
  expect(manhattan(new Point(0, 0), new Point(10, 0))).toEqual(10)
  expect(manhattan(new Point(0, 0), new Point(0, 10))).toEqual(10)
  expect(manhattan(new Point(2, 4), new Point(0, 6))).toEqual(4)
  expect(manhattan(new Point(-2, -2), new Point(4, 4))).toEqual(12)
  expect(manhattan(new Point(-2, 5), new Point(10, 6))).toEqual(13)
  expect(manhattan(new Point(0, 0), new Point(-2, -4))).toEqual(6)
})

test('combinations', () => {
  const gen = combinationGenerator([1, 2, 3])
  expect(gen.next().value).toEqual([1, 2, 3])
  expect(gen.next().value).toEqual([1, 3, 2])
  expect(gen.next().value).toEqual([2, 1, 3])
  expect(gen.next().value).toEqual([2, 3, 1])
  expect(gen.next().value).toEqual([3, 1, 2])
  expect(gen.next().value).toEqual([3, 2, 1])
  expect(gen.next().done).toEqual(true)
})

test('factorial', () => {
  expect(factorial(4)).toEqual(24)
  expect(factorial(1)).toEqual(1)
  expect(factorial(0)).toEqual(0)
  expect(factorial(-1)).toEqual(0)
})

test('termial', () => {
  expect(termial(4)).toEqual(10)
  expect(termial(3)).toEqual(6)
  expect(termial(2)).toEqual(3)
  expect(termial(1)).toEqual(1)
  expect(termial(0)).toEqual(0)
  expect(termial(-1)).toEqual(0)
})

test('gcd', () => {
  expect(gcd(20, 30)).toEqual(10)
  expect(gcd(42, 120, 285)).toEqual(3)
})

test('lcm', () => {
  expect(lcm(21, 42, 19)).toEqual(798)
})

test('shoelace', () => {
  expect(shoelace(
    new Point(0, 0),
    new Point(0, 2),
    new Point(2, 2),
    new Point(2, 0),
  )).toEqual(4)
  expect(shoelace(
    new Point(3,4),
    new Point(5,11),
    new Point(12,8),
    new Point(9,5),
    new Point(5,6),
  )).toEqual(30)
  expect(shoelace(
    new Point(2, 7),
    new Point(10, 1),
    new Point(8, 6),
    new Point(11, 7),
    new Point(7, 10),
  )).toEqual(32)
})
