import { readlines } from '../utils'

export type AlmanacMapRange = {
  destinationStart: number,
  sourceStart: number,
  sourceEnd: number,
  location?: number,
  transform: (number) => number
}

export type AlmanacMap = {
  type: string
  ranges: AlmanacMapRange[]
}

export type AlmanacMaps = {
  [key: string]: AlmanacMap
}

const parse = (inputPath: string): { seeds: number[], maps: AlmanacMaps } => {
  const lines = readlines(inputPath)
  const { groups: { seedsLine } } = /seeds:\s+(?<seedsLine>.*)/g.exec(lines.shift())
  const seeds = seedsLine.split(' ').map((n: string) => parseInt(n))
  const maps: AlmanacMaps = {}
  let currentMap: AlmanacMap = null
  for (const line of lines) {
    const startMatch = /(?<type>[a-z-]+) map:/.exec(line)
    const mapDataMatch = /(\d+\s?)/.exec(line)
    if (startMatch) {
      if (currentMap) {
        maps[currentMap.type] = currentMap
        currentMap = null
      }
      const { groups: { type } } = startMatch
      currentMap = { type, ranges: [] }
    } else if (mapDataMatch) {
      const [destinationStart, sourceStart, rangeSize] = line.split(' ').map((n: string) => parseInt(n))
      currentMap.ranges.push({
        destinationStart,
        sourceStart,
        sourceEnd: sourceStart + rangeSize - 1,
        transform: (source: number) => destinationStart + (source - sourceStart)
      })
    }
  }
  maps[currentMap.type] = currentMap
  for (const type in maps) {
    maps[type].ranges = maps[type].ranges.sort((a: AlmanacMapRange, b: AlmanacMapRange) => a.sourceStart - b.sourceStart)
  }

  return { seeds, maps }
}

export default parse
