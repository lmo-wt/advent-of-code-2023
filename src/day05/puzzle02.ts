import { min, samples } from '../utils'
import parse, { AlmanacMapRange } from './parse'

type Range = {
  start: number,
  end: number,
}

const solve = inputPath => {
  const { seeds, maps } = parse(inputPath)
  const seedRanges = samples(seeds, 2).map(([start, size]) => ({ start, end: start + size - 1})).sort((a: Range, b: Range) => a.start - b.start)
  const mapTargets = {
    'seed-to-soil': 'soil-to-fertilizer',
    'soil-to-fertilizer': 'fertilizer-to-water',
    'fertilizer-to-water': 'water-to-light',
    'water-to-light': 'light-to-temperature',
    'light-to-temperature': 'temperature-to-humidity',
    'temperature-to-humidity': 'humidity-to-location',
  }

  const splitSourceRange = (source: Range, ranges: AlmanacMapRange[]): Range[] => {
    if (ranges.length === 0) {
      return [source]
    }
    const nextRange = ranges[0]
    const destinationRanges = []
    if (source.start < nextRange.sourceStart) {
      destinationRanges.push({ start: source.start, end: source.end < nextRange.sourceStart ? source.end : nextRange.sourceStart - 1 })
    }
    if (source.end >= nextRange.sourceStart && source.start <= nextRange.sourceEnd) {
      destinationRanges.push({ start: nextRange.transform(source.start < nextRange.sourceStart ? nextRange.sourceStart: source.start), end: nextRange.transform(source.end < nextRange.sourceEnd ? source.end : nextRange.sourceEnd) })
    }
    if (source.end > nextRange.sourceEnd) {
      const remainingRanges = splitSourceRange({ start: source.start > nextRange.sourceEnd ? source.start : nextRange.sourceEnd + 1, end: source.end }, ranges.slice(1))
      destinationRanges.push(...remainingRanges)
    }

    return destinationRanges
  }

  const computeLocationsFromMapRange = (rangeType: string, source: Range): Range[] => {
    const ranges = maps[rangeType].ranges
    const destinationRanges = splitSourceRange(source, ranges)
    if (rangeType === 'humidity-to-location') {
      return destinationRanges
    } else {
      const transformedRanges = destinationRanges.map((range: Range) => computeLocationsFromMapRange(mapTargets[rangeType], range))
      return transformedRanges.flat()
    }
  }

  const locationRanges = seedRanges.map((seedRange: Range) => computeLocationsFromMapRange('seed-to-soil', seedRange))
  const solution = min(locationRanges.flat(), (range: Range) => range.start).start

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
