import { min } from '../utils'
import parse, { AlmanacMapRange } from './parse'

const solve = inputPath => {
  const { seeds, maps } = parse(inputPath)
  const mapTargets = {
    'seed-to-soil': 'soil-to-fertilizer',
    'soil-to-fertilizer': 'fertilizer-to-water',
    'fertilizer-to-water': 'water-to-light',
    'water-to-light': 'light-to-temperature',
    'light-to-temperature': 'temperature-to-humidity',
    'temperature-to-humidity': 'humidity-to-location',
  }
  const computeLocationFromMapRange = (rangeType: string, source: number): number => {
    const map = maps[rangeType]
    const range = map.ranges.find((range: AlmanacMapRange) => range.sourceStart <= source && range.sourceEnd >= source)
    if (range && range.location) {
      return range.location
    }
    const nextValue = range ? (range.transform(source)) : source
    if (rangeType === 'humidity-to-location') {
      return nextValue
    } else {
      return computeLocationFromMapRange(mapTargets[rangeType], nextValue)
    }
  }

  const locations = seeds.map((seed: number) => computeLocationFromMapRange('seed-to-soil', seed))
  const solution = min(locations)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
