import parse from './parse'

export const HASH = (input: string): number => {
  let result = 0
  for (let i = 0; i < input.length; i++) {
    const asciiCode = input.charCodeAt(i)
    result += asciiCode
    result *= 17
    result %= 256
  }
  return result
}

const solve = inputPath => {
  const steps = parse(inputPath)
  const solution = steps.reduce((acc, step) => {
    const hash = HASH(step)
    return acc + hash
  }, 0)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
