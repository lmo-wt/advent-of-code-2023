import parse from './parse'
import { HASH } from './puzzle01'

enum StepType {
  Remove = '-',
  Add = '=',
} 

type Step = {
  label: string
  type: StepType
  focal?: number
}

type Lens = {
  label: string
  focal: number
}

const solve = inputPath => {
  const steps = parse(inputPath).map(rawStep => {
    const match = /(?<label>[a-z]+)(?<type>[=-])(?<focal>[0-9]?)/.exec(rawStep)
    const { groups: { label, type } } = match
    if (type === StepType.Add) {
      const focal = match.groups.focal
      return { label, type: StepType.Add, focal: parseInt(focal) } as Step
    } else {
      return { label, type: StepType.Remove } as Step
    }
  })
  const boxes = new Array(256).fill(0).map(() => [])
  for (const step of steps) {
    const box = boxes[HASH(step.label)]
    const index = box.findIndex(lens => lens.label === step.label)
    if (step.type === StepType.Add) {
      const lens = { label: step.label, focal: step.focal } as Lens
      if (index >= 0) {
        box[index] = lens
      } else {
        box.push(lens)
      }
    } else {
      if (index >= 0) {
        box.splice(index, 1)
      }
    }
  }
  let solution = 0
  for (const [boxIndex, box] of boxes.entries()) {
    for (const [lensIndex, lens] of box.entries()) {
      const focusingPower = (boxIndex + 1) * (lensIndex + 1) * lens.focal
      solution += focusingPower
    }
  }
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
