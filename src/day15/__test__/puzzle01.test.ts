import solve, { HASH } from '../puzzle01'

test('HASH algorithm', () => {
  expect(HASH('HASH')).toEqual(52)
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(1320)
})
