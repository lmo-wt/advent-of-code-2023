import { readlines } from '../utils'

const parse = (inputPath: string): string[] => {
  const steps = readlines(inputPath)[0].split(',')
  return steps
}

export default parse
