import { Grid, makeGrid, readlines } from '../utils'

export type Tile = {
  heatloss: number
  distance: number
}

const parse = (inputPath: string): Grid<Tile> => {
  const values = readlines(inputPath).map((line: string) => line.split('').map((char: string) => ({ heatloss: parseInt(char), distance: Infinity })))
  const grid = makeGrid<Tile>(values)

  return grid
}

export default parse
