import { Signale } from 'signale'
import { Graph, Point, Vector, min } from '../utils'
import parse, { Tile } from './parse'
import * as memoize from 'memoizee'

export enum Direction {
  Up = '^',
  Down = 'v',
  Left = '<',
  Right = '>',
}

export const OppositeDirection = {
  [Direction.Up]: Direction.Down,
  [Direction.Down]: Direction.Up,
  [Direction.Left]: Direction.Right,
  [Direction.Right]: Direction.Left,
}

export const LeftDirection = {
  [Direction.Up]: Direction.Left,
  [Direction.Down]: Direction.Right,
  [Direction.Left]: Direction.Down,
  [Direction.Right]: Direction.Up,
}

export const RightDirection = {
  [Direction.Up]: Direction.Right,
  [Direction.Down]: Direction.Left,
  [Direction.Left]: Direction.Up,
  [Direction.Right]: Direction.Down,
}

export const DirectionVector = {
  [Direction.Up]: new Vector(0, 1),
  [Direction.Down]: new Vector(0, -1),
  [Direction.Left]: new Vector(-1, 0),
  [Direction.Right]: new Vector(1, 0),
}

const solve = (inputPath: string, minimumSteps = 1, maximumSteps = 3) => {
  const grid = parse(inputPath)
  const solution = 0
  const signale  = new Signale({ interactive: true })
  
  const dijkstra = (from: Point): void => {
    const tile = grid.get(from.x, from.y)
    for (const direction of [Direction.Up, Direction.Down, Direction.Left, Direction.Right]) {
      const neighbour = from.translate(DirectionVector[direction])
      if (!grid.isOOB(neighbour.x, neighbour.y)) {
        const neighbourTile = grid.get(neighbour.x, neighbour.y)
        const distance = tile.distance + tile.heatloss
        if (distance < neighbourTile.distance) {
          neighbourTile.distance = distance
          dijkstra(neighbour)
        }
      }
    }
  }
  grid.get(grid.xmax, 0).distance = 0
  dijkstra(new Point(grid.xmax, 0))
  grid.get(0, grid.ymax).heatloss = 0

  const hashToPoint = (hash: string): Point => {
    const [x, y] = hash.slice(1).split('y').map((value: string) => parseInt(value))
    return new Point(x, y)
  }

  let minHeatloss = Infinity
  const increment = 0
  const compute = memoize((point: Point, previousDirection: Direction, path: Set<string> = new Set(), currentHeatLoss = 0) => {
    if (path.has(point.hash())) {
      return
    }
    const tile = grid.get(point.x, point.y)
    let cumulatedHeatloss = currentHeatLoss + tile.heatloss
    if (point.x === grid.xmax && point.y === 0) {
      if (cumulatedHeatloss < minHeatloss) {
        minHeatloss = cumulatedHeatloss
        signale.info('Current heatloss = %d. Path was %s', minHeatloss, [...path.values()].join('->'))
        const points = [...path.values()]
          .map((hash: string) => hashToPoint(hash))
        const totalHeatloss = points.reduce((acc, point) => acc + grid.get(point.x, point.y).heatloss, 0)
        if (totalHeatloss !== cumulatedHeatloss) {
          // throw new Error('Total heatloss does not match')
        }
      }
      return
    }
    if (cumulatedHeatloss + tile.distance > minHeatloss) {
      return
    }
    for (const side of ['left', 'right']) {
      path.add(point.hash())
      const neighbours = []
      const direction = side === 'left' ? LeftDirection[previousDirection] : RightDirection[previousDirection]
      for (let i = minimumSteps; i <= maximumSteps; i += 1) {
        const neighbour = point.translate(DirectionVector[direction].multiply(i))
        if (!grid.isOOB(neighbour.x, neighbour.y) && !path.has(neighbour.hash())) {
          const neighbourTile = grid.get(neighbour.x, neighbour.y)
          if (cumulatedHeatloss + neighbourTile.distance > minHeatloss) {
            return
          }
          compute(neighbour, direction, new Set(path), cumulatedHeatloss)
          path.add(neighbour.hash())
          cumulatedHeatloss += neighbourTile.heatloss
        } else {
          break
        }
      }
    }
  })

  compute(new Point(0, grid.ymax), Direction.Down)
  // compute(new Point(0, grid.ymax), Direction.Left)
  return minHeatloss
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/__test__/input.txt`)
  console.log(`The solution is ${solution}`)
}
