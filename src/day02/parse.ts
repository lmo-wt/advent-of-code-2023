import { readlines } from '../utils'

export type Set = {
  [key: string]: number
}

export type Game = {
  id: string
  sets: Set[]
}

const parse = (inputPath: string): Game[] => {
  const games = readlines(inputPath).map((line: string) => {
    const { groups: { id, game } } = /Game (?<id>\d+): (?<game>.*)/g.exec(line)
    const sets: Set[] = game.split('; ').map(cubes => {
      const set = { 'green': 0, 'red': 0, 'blue': 0 }
      for (const cube of cubes.split(', ')) {
        const { groups: { color, quantity } } = /(?<quantity>\d+) (?<color>[a-z]+)/.exec(cube)
        set[color] = parseInt(quantity)
      }
      return set
    })


    return { id, sets } as Game
  })

  return games
}

export default parse
