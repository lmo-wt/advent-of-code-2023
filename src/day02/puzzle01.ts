import { every } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const RED = 12
  const GREEN = 13
  const BLUE = 14

  const games = parse(inputPath)
  const possibleGames = games.filter(game => every(game.sets, set => set.red <= RED && set.green <= GREEN && set.blue <= BLUE))
  const solution = possibleGames.reduce((acc, game) => acc + parseInt(game.id), 0)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
