import { sum } from '../utils'
import parse, { Set } from './parse'

const solve = inputPath => {
  const games = parse(inputPath)
  const colors = ['red', 'green', 'blue']
  const powers = games.map(game => {
    const minimumSet: Set = game.sets[0]
    for (const set of game.sets.slice(1)) {
      for (const color of colors) {
        minimumSet[color] = Math.max(minimumSet[color], set[color])
      }
    }
    return minimumSet['blue'] * minimumSet['green'] * minimumSet['red']
  })
  const solution = sum(powers)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
