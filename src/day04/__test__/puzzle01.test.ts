import solve from '../puzzle01'

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(13)
})
