import parse from './parse'

const solve = inputPath => {
  const cards = parse(inputPath)

  let points = 0
  for (const card of cards) {
    const scratchedWinning = card.scratchedNumbers.filter(n => card.winningNumbers.includes(n))
    if (scratchedWinning.length > 0) {
      points += Math.pow(2, scratchedWinning.length - 1)
    }
  }

  return points
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
