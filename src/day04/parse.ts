import { readlines } from '../utils'

export type Card = {
  id: number
  winningNumbers: number[]
  scratchedNumbers: number[]
}

const parseNumbers = (numbers: string): number[] => {
  return numbers.replaceAll(/\s+/g, ' ').split(' ').map((n: string) => parseInt(n.trim()))
}

const parse = (inputPath: string): Card[] => {
  const cards = readlines(inputPath).map((line: string) => {
    const { groups: { id, rest } } = /Card\s+(?<id>\d+):\s+(?<rest>.*)/g.exec(line)
    const [winningNumbers, scratchedNumbers] = rest.split(' | ').map(parseNumbers)
    return {
      id: parseInt(id),
      winningNumbers,
      scratchedNumbers
    }
  })

  return cards
}

export default parse
