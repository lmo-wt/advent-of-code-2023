import { sum } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const cards = parse(inputPath)
  const cardsQuantities = cards.map(() => 1)

  for (let i = 0; i < cards.length; i += 1) {
    const card = cards[i]
    const scratchedWinning = card.scratchedNumbers.filter(n => card.winningNumbers.includes(n)).length
    for (let j = i + 1; j <= i + scratchedWinning; j += 1) {
      cardsQuantities[j] += cardsQuantities[i]
    }
  }

  return sum(cardsQuantities)
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
