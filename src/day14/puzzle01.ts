import { Grid, Point, Vector } from '../utils'
import parse, { Rock } from './parse'

export enum Direction {
  North = 'north',
  East = 'east',
  South = 'south',
  West = 'west',
}

const DirectionVectors = {
  [Direction.North]: { x: 0, y: 1 } as Vector,
  [Direction.East]: { x: 1, y: 0 } as Vector,
  [Direction.South]: { x: 0, y: -1 } as Vector,
  [Direction.West]: { x: -1, y: 0 } as Vector,
}

export const tilt = (grid: Grid<Rock>, direction: Direction = Direction.North): Grid<Rock> => {
  for (let x = direction === Direction.East ? grid.xmax : 0
    ; direction === Direction.East ? x >= 0 : x <= grid.xmax
    ; x += direction === Direction.East ? -1 : 1)
  {
    for (let y = direction === Direction.North ? grid.ymax : 0
      ; direction === Direction.North ? y >= 0 : y <= grid.ymax 
      ; y += direction === Direction.North ? -1 : 1)
    {
      const tile = grid.get(x, y)
      if (tile === Rock.Round) {
        let position = new Point(x, y)
        let nextPosition = position.translate(DirectionVectors[direction])
        while (nextPosition.x >= 0 && nextPosition.x <= grid.xmax
          && nextPosition.y >= 0 && nextPosition.y <= grid.ymax
          && grid.get(nextPosition.x, nextPosition.y) === Rock.Empty
        ) {
          position = nextPosition
          nextPosition = nextPosition.translate(DirectionVectors[direction])
        }
        grid.set(x, y, Rock.Empty)
        grid.set(position.x, position.y, Rock.Round)
      }
    }
  }
  return grid
}

export const calculateLoad = (grid: Grid<Rock>): number => {
  let load = 0
  grid.iterate((tile, x, y) => {
    if (tile === Rock.Round) {
      load += y + 1
    }
  })
  return load
}

const solve = inputPath => {
  const grid = parse(inputPath)
  tilt(grid, Direction.North)
  const solution = calculateLoad(grid)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
