import { Grid } from '../utils'
import parse, { Rock } from './parse'
import { tilt, Direction, calculateLoad } from './puzzle01'

const getState = (grid: Grid<Rock>): string => {
  return grid.values.map(line => line.join('')).join('')
}

const solve = inputPath => {
  const grid = parse(inputPath)
  const states = new Map<string, number>()
  const iterations = 1000000000
  let jumped = false
  for (let i = 0; i < iterations; i += 1) {
    tilt(grid, Direction.North)
    tilt(grid, Direction.West)
    tilt(grid, Direction.South)
    tilt(grid, Direction.East)
    const state = grid.values.map(line => line.join('')).join('')
    if (states.has(state) && !jumped) {
      const previousIndex = states.get(state)
      const loopSize = i - previousIndex
      const remainingIterations = iterations - i
      const loopsToPass = Math.floor(remainingIterations / loopSize)
      i += loopsToPass * loopSize
      jumped = true
    }
    states.set(getState(grid), i)
  }
  const solution = calculateLoad(grid)
  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
