import { Grid, makeGrid, readlines } from '../utils'

export enum Rock {
  Empty = '.',
  Cube = '#',
  Round = 'O',
}

const parse = (inputPath: string): Grid<Rock> => {
  const values = readlines(inputPath).map((line: string) => line.split('') as Rock[])
  const grid = makeGrid<Rock>(values, false, (value: Rock) => value)

  return grid
}

export default parse
