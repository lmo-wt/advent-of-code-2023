import solve from '../puzzle01'

test('solves test input01', () => {
  const solution = solve(`${__dirname}/input01.txt`)
  expect(solution).toEqual(4361)
})

test('solves test input02', () => {
  const solution = solve(`${__dirname}/input02.txt`)
  expect(solution).toEqual(38779)
})

test('solves test input03', () => {
  const solution = solve(`${__dirname}/input03.txt`)
  expect(solution).toEqual(6796)
})

test('solves test input04', () => {
  const solution = solve(`${__dirname}/input04.txt`)
  expect(solution).toEqual(531932)
})
