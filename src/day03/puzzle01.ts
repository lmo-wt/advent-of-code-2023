import { sum } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const { parts, grid } = parse(inputPath)
  const realParts = parts.filter(part => {
    return part.coordinates.find(coordinate => {
      const hasSymbolNeighbour = (coordinate.x !== 0 && grid.get(coordinate.x - 1, coordinate.y) !== '.')
        || (coordinate.y !== 0 && grid.get(coordinate.x, coordinate.y - 1) !== '.')
        || (coordinate.x !== grid.xmax && grid.get(coordinate.x + 1, coordinate.y) !== '.')
        || (coordinate.y !== grid.ymax && grid.get(coordinate.x, coordinate.y + 1) !== '.')
        || (coordinate.x !== 0 && coordinate.y !== 0 && grid.get(coordinate.x - 1, coordinate.y - 1) !== '.')
        || (coordinate.x !== 0 && coordinate.y !== grid.ymax && grid.get(coordinate.x - 1, coordinate.y + 1) !== '.')
        || (coordinate.x !== grid.xmax && coordinate.y !== 0 && grid.get(coordinate.x + 1, coordinate.y - 1) !== '.')
        || (coordinate.x !== grid.xmax && coordinate.y !== grid.ymax && grid.get(coordinate.x + 1, coordinate.y + 1) !== '.')
      return hasSymbolNeighbour
    })
  })
  const solution = sum(realParts, part => part.number)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
