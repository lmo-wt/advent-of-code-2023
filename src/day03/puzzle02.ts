import { Point, sum } from '../utils'
import parse from './parse'

const solve = inputPath => {
  const { parts, grid, gears } = parse(inputPath)
  const getCloseParts = (gear: Point) => {
    const closeParts = []
    for (const part of parts) {
      const isClose = part.coordinates.find(coordinate => {
        return (coordinate.x !== 0 && gear.x === coordinate.x - 1 && gear.y === coordinate.y)
          || (coordinate.y !== 0 && gear.x === coordinate.x && gear.y === coordinate.y - 1)
          || (coordinate.x !== grid.xmax && gear.x === coordinate.x + 1 && gear.y === coordinate.y)
          || (coordinate.y !== grid.ymax && gear.x === coordinate.x && gear.y === coordinate.y + 1)
          || (coordinate.x !== 0 && coordinate.y !== 0 && gear.x === coordinate.x - 1 && gear.y === coordinate.y - 1)
          || (coordinate.x !== 0 && coordinate.y !== grid.ymax && gear.x === coordinate.x - 1 && gear.y === coordinate.y + 1)
          || (coordinate.x !== grid.xmax && coordinate.y !== 0 && gear.x === coordinate.x + 1 && gear.y === coordinate.y - 1)
          || (coordinate.x !== grid.xmax && coordinate.y !== grid.ymax && gear.x === coordinate.x + 1 && gear.y === coordinate.y + 1)
      })
      if (isClose) {
        closeParts.push(part)
        if (closeParts.length > 2) {
          break
        }
      }
    }
    return closeParts
  }
  const ratios = gears.map(gear => {
    const closeParts = getCloseParts(gear)
    return closeParts.length === 2 ? closeParts[0].number * closeParts[1].number : 0
  })


  const solution = sum(ratios)

  return solution
}

export default solve

/* istanbul ignore next */
if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
