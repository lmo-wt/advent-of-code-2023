import { Grid, Point, makeGrid, readlines } from '../utils'

export type Part = {
  number: number
  coordinates: Point[]
}

const parse = (inputPath: string): { parts: Part[], grid: Grid<string>, gears: Point[] } => {
  const grid = makeGrid(readlines(inputPath).map((line: string) => {
    return line.split('')
  }).reverse())
  const parts: Part[] = []
  const gears: Point[] = []
  let currentPart = null
  for (let y = 0; y <= grid.ymax; y++) {
    if (currentPart) {
      parts.push(currentPart)
      currentPart = null
    }
    for (let x = 0; x <= grid.xmax; x++) {
      const value = grid.get(x, y)
      const number = parseInt(grid.get(x, y))
      if (!isNaN(number)) {
        if (!currentPart) {
          currentPart = { number, coordinates: [{ x, y } as Point] } as Part
        } else {
          currentPart.coordinates.push({ x, y } as Point)
          currentPart.number = parseInt(`${currentPart.number}${number}`)
        }
        grid.set(x, y, '.')
      } else {
        if (currentPart) {
          parts.push(currentPart)
          currentPart = null
        }
        if (value === '*') {
          gears.push({ x, y } as Point)
        }
      }
    }
  }

  return { parts, grid, gears }
}

export default parse
