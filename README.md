[![pipeline status](https://gitlab.wizardstech.io/lmo/advent-of-code-2023/badges/main/pipeline.svg)](https://gitlab.wizardstech.io/lmo/advent-of-code-2023/commits/main)
[![coverage report](https://gitlab.wizardstech.io/lmo/advent-of-code-2023/badges/main/coverage.svg)](https://gitlab.wizardstech.io/lmo/advent-of-code-2023/commits/main)

# Advent of Code

## 2023

[https://adventofcode.com/2023](https://adventofcode.com/2023)
